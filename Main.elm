module Main exposing (..)
import Html

add a b =
  a + b

result =
  -- add (add 1 2) 3
  -- add 1 2 |> add 3

  -- false, uneven
  -- add 1 2 |> \a -> a % 2 == 0
  -- true, even
  add 2 2 |> \a -> a % 2 == 0

counter =
  1

increment counter amount =
  -- counter + amount
  let -- assign variables in let
    localCount =
      counter
  in  -- access to variables defined in let
    localCount + amount -- return

sum =
  increment counter 5

main =
  Html.text(toString sum)

